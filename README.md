# frontend_vue

## Project setup
```
yarn install
```

### Instalação do cliente "Http" AXIOS para acesso a API de Backend
```
yarn install axios --save
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# ERROS E SOLUÇÕES
## Argument of type '{ name: string; props: { msg: String Constructor; }; }' is not assignable to parameter of type 'new (...args: any[]) => any'
SOLUÇÃO

    Rodar o comando : npm i @types/vuelidate --save-dev
    Após o procedimento acima deletar a pasta "Node Modules" e o dirtório ".lock" 


    Rodar o comando "npm install"